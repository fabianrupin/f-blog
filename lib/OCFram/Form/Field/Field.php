<?php

namespace OCFram\Form\Field;

use OCFram\Form\Validator;
use OCFram\Hydrator;

abstract class Field
{
    use Hydrator;

    /** @var string */
    protected $errorMessage;

    /** @var  string */
    protected $label;

    /** @var  string */
    protected $name;

    /** @var array */
    protected $validators = [];

    /** @var  string */
    protected $value;

    /**
     * Field constructor.
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        if (!empty($options)) {
            $this->hydrate($options);
        }
    }

    /**
     * @return mixed
     */
    abstract public function buildWidget();

    /**
     * @return bool
     */
    public function isValid()
    {
        foreach ($this->validators as $validator) {
            if (!$validator->isValid($this->value)) {
                $this->errorMessage = $validator->errorMessage();
                return false;
            }
        }

        return true;
    }

    public function label()
    {
        return $this->label;
    }

    public function name()
    {
        return $this->name;
    }

    public function validators()
    {
        return $this->validators;
    }

    public function value()
    {
        return $this->value;
    }

    public function setLabel($label)
    {
        if (is_string($label)) {
            $this->label = $label;
        }
    }

    public function setName($name)
    {
        if (is_string($name)) {
            $this->name = $name;
        }
    }

    public function setValidators(array $validators)
    {
        foreach ($validators as $validator) {
            if ($validator instanceof Validator && !in_array($validator, $this->validators)) {
                $this->validators[] = $validator;
            }
        }
    }

    public function setValue($value)
    {
        if (is_string($value)) {
            $this->value = $value;
        }
    }
}