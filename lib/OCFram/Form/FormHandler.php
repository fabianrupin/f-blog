<?php

namespace OCFram\Form;

use OCFram\Database\Manager;
use OCFram\HTTPFoundation\HTTPRequest;

class FormHandler
{
    /** @var  Form */
    protected $form;

    /** @var  Manager */
    protected $manager;

    /** @var  HTTPRequest */
    protected $request;

    /**
     * FormHandler constructor.
     * @param Form $form
     * @param Manager $manager
     * @param HTTPRequest $request
     */
    public function __construct(Form $form, Manager $manager, HTTPRequest $request)
    {
        $this->setForm($form);
        $this->setManager($manager);
        $this->setRequest($request);
    }

    /**
     * @return bool
     */
    public function process()
    {
        if ($this->request->method() == 'POST' && $this->form->isValid()) {
            $this->manager->save($this->form->entity());

            return true;
        }

        return false;
    }

    /**
     * @param Form $form
     * @return void
     */
    public function setForm(Form $form)
    {
        $this->form = $form;
    }

    /**
     * @param Manager $manager
     * @return void
     */
    public function setManager(Manager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param HTTPRequest $request
     * @return void
     */
    public function setRequest(HTTPRequest $request)
    {
        $this->request = $request;
    }
}