<?php

namespace OCFram\Form;

use OCFram\Model\Entity;

abstract class FormBuilder
{
    /** @var  Form */
    protected $form;

    /**
     * FormBuilder constructor.
     * @param Entity $entity
     */
    public function __construct(Entity $entity)
    {
        $this->setForm(new Form($entity));
    }

    abstract public function build();

    public function setForm(Form $form)
    {
        $this->form = $form;
    }

    public function form()
    {
        return $this->form;
    }
}