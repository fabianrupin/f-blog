<?php

namespace OCFram\Form;

use OCFram\Form\Field\Field;
use OCFram\Model\Entity;

class Form
{
    /** @var  Entity */
    protected $entity;
    /** @var array */
    protected $fields = [];

    /**
     * Form constructor.
     * @param Entity $entity
     */
    public function __construct(Entity $entity)
    {
        $this->setEntity($entity);
    }

    /**
     * add a field to a comment
     *
     * @param Field $field
     * @return Form $this
     */
    public function add(Field $field)
    {
        $attr = $field->name(); // On récupère le nom du champ.
        $field->setValue($this->entity->$attr()); // On assigne la valeur correspondante au champ.

        $this->fields[] = $field; // On ajoute le champ passé en argument à la liste des champs.
        return $this;
    }

    /**
     * Generate every field in a string
     *
     * @return string
     */
    public function createView()
    {
        $view = '';

        foreach ($this->fields as $field) {
            $view .= $field->buildWidget() . '<br />';
        }

        return $view;
    }

    /**
     * Check if fields are valids
     *
     * @return bool
     */
    public function isValid()
    {
        $valid = true;

        /** @var Field $field */
        foreach ($this->fields as $field) {
            if (!$field->isValid()) {
                $valid = false;
            }
        }

        return $valid;
    }

    public function entity()
    {
        return $this->entity;
    }

    public function setEntity(Entity $entity)
    {
        $this->entity = $entity;
    }
}