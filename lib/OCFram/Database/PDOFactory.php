<?php

namespace OCFram\Database;

class PDOFactory
{
    /**
     * @return \PDO
     */
    public static function getMysqlConnexion()
    {
        $db = new \PDO('mysql:host=localhost;dbname=news', 'root', '');
        $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        return $db;
    }
}