<?php

namespace OCFram\Database;

abstract class Manager
{
    protected $dao;

    /**
     * Manager constructor.
     * @param $dao
     */
    public function __construct(\PDO $dao)
    {
        $this->dao = $dao;
    }
}