<?php

use OCFram\SplClassLoader;

const DEFAULT_APP = 'Frontend';

// Si l'application n'est pas valide, on va charger l'application par défaut qui se chargera de générer une erreur 404
if (!isset($_GET['app']) || !file_exists(__DIR__ . '/../src/' . $_GET['app'])) $_GET['app'] = DEFAULT_APP;

// On commence par inclure la classe nous permettant d'enregistrer nos autoload
require __DIR__ . '/../lib/OCFram/SplClassLoader.php';

// On va ensuite enregistrer les autoloads correspondant à chaque vendor (OCFram, src, Manager, etc.)
$OCFramLoader = new SplClassLoader('OCFram', __DIR__ . '/../lib');
$OCFramLoader->register();

$managerLoader = new SplClassLoader('Manager', __DIR__ . '/../src');
$managerLoader->register();

$entityLoader = new SplClassLoader('Entity', __DIR__ . '/../src');
$entityLoader->register();

$frontendLoader = new SplClassLoader('Frontend', __DIR__ . '/../src');
$frontendLoader->register();

$backendLoader = new SplClassLoader('Backend', __DIR__ . '/../src');
$backendLoader->register();

$FormBuilderLoader = new SplClassLoader('FormBuilder', __DIR__ . '/../src');
$FormBuilderLoader->register();

// Il ne nous suffit plus qu'à déduire le nom de la classe et à l'instancier
$appClass = $_GET['app'] . '\\' . $_GET['app'] . 'Application';

/** @var \OCFram\Application $app */
$app = new $appClass;
$app->run();
