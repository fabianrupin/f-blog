<?php

namespace Frontend;

use OCFram\Application;

class FrontendApplication extends Application
{
    /**
     * FrontendApplication constructor.
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->name = 'Frontend';
    }

    /**
     * return void
     */
    public function run()
    {
        $controller = $this->getController();
        $controller->execute();

        $this->httpResponse->setPage($controller->page());
        $this->httpResponse->send();
    }
}