<?php

namespace Frontend\Modules\News;

use Entity\News;
use OCFram\BackController;
use Entity\Comment;
use FormBuilder\CommentFormBuilder;
use OCFram\Form\FormHandler;
use OCFram\HTTPFoundation\HTTPRequest;

class NewsController extends BackController
{
    /**
     * @param HTTPRequest $request
     * @return void
     */
    public function executeIndex(HTTPRequest $request)
    {
        /** @var int $nombreNews */
        $nombreNews = $this->app->config()->get('nombre_news');

        /** @var int $nombreCaracteres */
        $nombreCaracteres = $this->app->config()->get('nombre_caracteres');

        // On ajoute une définition pour le titre.
        $this->page->addVar('title', 'Liste des ' . $nombreNews . ' dernières news');

        // On récupère le manager des news.
        $manager = $this->managers->getManagerOf('News');

        $listeNews = $manager->getList(0, $nombreNews);

        /** @var News $news */
        foreach ($listeNews as $news) {
            if (strlen($news->contenu()) > $nombreCaracteres) {
                $debut = substr($news->contenu(), 0, $nombreCaracteres);
                $debut = substr($debut, 0, strrpos($debut, ' ')) . '...';
                $news->setContenu($debut);
            }
        }

        // On ajoute la variable $listeNews à la vue, avec le tableau $listeNews contenant les News récupérées
        $this->page->addVar('listeNews', $listeNews);
    }

    /**
     * Get wanted news with its comments
     *
     * @param HTTPRequest $request
     * @return void
     */
    public function executeShow(HTTPRequest $request)
    {
        /** @var News $news */
        $news = $this->managers->getManagerOf('News')->getUnique($request->getData('id'));

        if (empty($news)) {
            $this->app->httpResponse()->redirect404();
        }

        $this->page->addVar('title', $news->titre());
        $this->page->addVar('news', $news);
        $this->page->addVar('comments', $this->managers->getManagerOf('Comments')->getListOf($news->id()));
    }

    /**
     * Create a new CommentForm, create Comment Object with postData, redirect to this news
     *
     * @param HTTPRequest $request
     * @return void
     */
    public function executeInsertComment(HTTPRequest $request)
    {
        // Si le formulaire a été envoyé.
        if ($request->method() == 'POST') {
            $comment = new Comment([
                'news' => $request->getData('news'),
                'auteur' => $request->postData('auteur'),
                'contenu' => $request->postData('contenu')
            ]);
        } else {
            $comment = new Comment;
        }

        $formBuilder = new CommentFormBuilder($comment);
        $formBuilder->build();

        $form = $formBuilder->form();

        $formHandler = new FormHandler($form, $this->managers->getManagerOf('Comments'), $request);

        if ($formHandler->process()) {
            $this->app->user()->setFlash('Le commentaire a bien été ajouté, merci !');

            $this->app->httpResponse()->redirect('news-' . $request->getData('news') . '.html');
        }

        $this->page->addVar('comment', $comment);
        $this->page->addVar('form', $form->createView());
        $this->page->addVar('title', 'Ajout d\'un commentaire');
    }
}