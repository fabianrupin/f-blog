<?php

namespace Manager;

use OCFram\Database\Manager;
use Entity\News;

abstract class NewsManager extends Manager
{
    /**
     * Add a news
     *
     * @param $news News
     * @return void
     */
    abstract protected function add(News $news);

    /**
     * Save a news
     *
     * @param $news News
     * @see self::add()
     * @see self::modify()
     * @return void
     */
    public function save(News $news)
    {
        if ($news->isValid()) {
            $news->isNew() ? $this->add($news) : $this->modify($news);
        } else {
            throw new \RuntimeException('La news doit être validée pour être enregistrée');
        }
    }

    /**
     * Give number of news in database
     * @return int
     */
    abstract public function count();

    /**
     * Delete a news
     *
     * @param $id int
     * @return void
     */
    abstract public function delete($id);

    /**
     * Give a news list define with end Id and start Id
     *
     * @param int $debut int La première news à sélectionner
     * @param int $limite int Le nombre de news à sélectionner
     * @return array La liste des news. Chaque entrée est une instance de News.
     */
    abstract public function getList($debut = -1, $limite = -1);

    /**
     * Return a specific news
     *
     * @param $id int L'identifiant de la news à récupérer
     * @return News news ask
     */
    abstract public function getUnique($id);

    /**
     * Modify a news
     *
     * @param $news news
     * @return void
     */
    abstract protected function modify(News $news);
}