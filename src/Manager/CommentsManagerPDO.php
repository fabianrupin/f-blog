<?php

namespace Manager;

use Entity\Comment;

class CommentsManagerPDO extends CommentsManager
{
    /**
     * Add a new comment
     *
     * @param Comment $comment
     * @return void
     */
    protected function add(Comment $comment)
    {
        $q = $this->dao->prepare('INSERT INTO comments SET news = :news, auteur = :auteur, contenu = :contenu, date = NOW()');

        $q->bindValue(':news', $comment->news(), \PDO::PARAM_INT);
        $q->bindValue(':auteur', $comment->auteur());
        $q->bindValue(':contenu', $comment->contenu());

        $q->execute();

        $comment->setId($this->dao->lastInsertId());
    }

    /**
     * Delete a comment
     *
     * @param int $id Comment id
     * @return void
     */
    public function delete($id)
    {
        $this->dao->exec('DELETE FROM comments WHERE id = ' . (int)$id);
    }

    /**
     * Delete comments associate with a specific news
     *
     * @param int $id news id
     * @return void
     */
    public function deleteFromNews($id)
    {
        $this->dao->exec('DELETE FROM comments WHERE news = ' . (int)$id);
    }

    /**
     * Get comments associate with a specific news
     *
     * @param int $id news id
     * @return array with Comments object
     */
    public function getListOf($id)
    {
        if (!ctype_digit($id)) {
            throw new \InvalidArgumentException('L\'identifiant de la news passé doit être un nombre entier valide');
        }

        $q = $this->dao->prepare('SELECT id, news, auteur, contenu, date FROM comments WHERE news = :news');
        $q->bindValue(':news', $id, \PDO::PARAM_INT);
        $q->execute();

        $q->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Entity\Comment');

        $comments = $q->fetchAll();

        /** @var Comment $comment */
        foreach ($comments as $comment) {
            $comment->setDate(new \DateTime($comment->date()));
        }

        return $comments;
    }

    /**
     * Modify a comment
     *
     * @param Comment $comment
     * return void
     */
    protected function modify(Comment $comment)
    {
        $q = $this->dao->prepare('UPDATE comments SET auteur = :auteur, contenu = :contenu WHERE id = :id');

        $q->bindValue(':auteur', $comment->auteur());
        $q->bindValue(':contenu', $comment->contenu());
        $q->bindValue(':id', $comment->id(), \PDO::PARAM_INT);

        $q->execute();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function get($id)
    {
        $q = $this->dao->prepare('SELECT id, news, auteur, contenu FROM comments WHERE id = :id');
        $q->bindValue(':id', (int)$id, \PDO::PARAM_INT);
        $q->execute();

        $q->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Entity\Comment');

        return $q->fetch();
    }
}