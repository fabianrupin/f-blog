<?php

namespace Manager;

use OCFram\Database\Manager;
use Entity\Comment;

abstract class CommentsManager extends Manager
{
    /**
     * Abstract Method which add a new comment
     *
     * @param Comment $comment
     * @return void
     */
    abstract protected function add(Comment $comment);

    /**
     * Abstract Method which delete a comment
     *
     * @param int $id comment id
     * @return void
     */
    abstract public function delete($id);

    /**
     * Delete all comments associate with a news
     *
     * @param int $id news id
     * @return void
     */
    abstract public function deleteFromNews($id);

    /**
     * Save a new comment
     *
     * @param Comment $comment
     * @return void
     */
    public function save(Comment $comment)
    {
        if ($comment->isValid()) {
            $comment->isNew() ? $this->add($comment) : $this->modify($comment);
        } else {
            throw new \RuntimeException('Le commentaire doit être validé pour être enregistré');
        }
    }

    /**
     * get a list of comment of specific news
     *
     * @param int $id news id
     * @return array
     */
    abstract public function getListOf($id);

    /**
     * modify a comment
     *
     * @param Comment $comment
     * @return void
     */
    abstract protected function modify(Comment $comment);

    /**
     * get a specific comment
     *
     * @param $id comment id
     * @return Comment
     */
    abstract public function get($id);
}